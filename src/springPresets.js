'use strict';

module.exports = {
  wobble: [120, 13],
  gentleNoWobble: [186, 14],
  noWobble: [276, 24],
  reveal: [44, 44],
};
