import React, {Component} from 'react';
import Hero from '../components/Hero';
import MediaVideo from '../components/MediaVideo';
import OurOfferSection from "../components/OurOfferSection";
import OurPartners from '../components/OurPartners';
import PropTypes from 'prop-types';
import SectionOpening from '../components/SectionOpening';
import TeamListComponent from '../components/TeamListComponent';


class Homepage extends Component {
  static contextTypes = {
    currentMedia: PropTypes.string,
  };

  static propTypes = {
    location: PropTypes.object,
  };

  render() {
    return (
      <div
        role="main"
        className="dt-page dt-home-page"
      >
        <Hero />
        <main className="main-section">
          <div className="portfolio-wrapper">
            <MediaVideo
              videoNumber="1"
              videoTitle="Temple of Dimensions 4K"
              videoPosterPreview="dim-4k"
              videoUrl="https://www.youtube.com/watch?v=rh0WKka2Uyg"
            />
            <MediaVideo
              videoNumber="2"
              videoTitle="Paweł Kowalczyk - Weapons"
              videoPosterPreview="pawel-kowalczyk-weapons"
              videoUrl="https://google.com/"
            />
            <MediaVideo
              videoNumber="3"
              videoTitle="Tomasz Koszański - Movement"
              videoPosterPreview="tomasz-koszanski-movement"
              videoUrl="https://www.youtube.com/watch?v=VEcXX7Cf43g"
            />
            <MediaVideo
              videoNumber="4"
              videoTitle="Temple of Dimensions 4K"
              videoPosterPreview="dim-4k"
              videoUrl="https://www.youtube.com/watch?v=rh0WKka2Uyg"
            />
          </div>
          <div className="regular-section white partners-section">
            <SectionOpening sectionTitle="Partnerzy">
              Oferujemy szeroką gamę pokazów oraz działalność
              promocyjną w dziedzinie sportu, sztuki i filmu.
            </SectionOpening>
            <OurPartners />
            <img
              className="partners-accent-finisher"
              src="/img/svg/partners.svg"
              alt="Partnerzy"
              aria-hidden="true"
            />
          </div>
          <div className="regular-section black trickers-section">
            <SectionOpening sectionTitle="Trickerzy">
              Może nasz kojarzysz jako gości skaczących wszystko z jednej
              nogi, uwielbiamy kopać, kręcić dwójki, bawić się ruchem i
              tworzyć nowe kombinacje.
            </SectionOpening>
            <TeamListComponent />
            <img
              className="team-accent-finisher"
              src="/img/svg/team.svg"
              alt="Team"
            />
          </div>
          <div className="regular-section white">
            <div className="offers-wrapper">
              <OurOfferSection
                className="commercial-offer"
                offerType="Komercyjna"
                offerImage="/img/offer-placeholder-sm.jpg"
              >
                <p className="offer-content-heading">
                  Produkcja
                </p>
                <p className="offer-content-text">
                  Tworzymy filmy promocyjne oraz tematyczne przedstawiające nasz
                  sport Tricking, tworzone przez nas choreografie walk, nurt
                  Movement oraz tutoriale z wykonywanych przez nas elementów.
                  Udzielamy się zarówno przed kamerami jako kaskaderzy i aktorzy
                  jak i za nimi zajmując się produkcją filmową od nagrywania po
                  edycje filmów.
                </p>
                <br /><br />
                <p className="offer-content-heading">
                  Eventy
                </p>
                <p className="offer-content-text">
                  Tworzymy eventy oraz warsztaty tematyczne, skupiające się na promocji
                  sportu oraz dzieleniu się wiedzą z zakresu wykonywanych przez nas ruchów.
                  Występujemy w formie organizatorów oraz współtwórców.
                  Jesteśmy również otwarci na współpracę przy eventach organizowanyc
                  przez was jako grupa pokazowa jednocząca siły z akrobatami i tancerzami.
                </p>
              </OurOfferSection>
              <OurOfferSection
                className="training-offer"
                offerType="Treningowa"
                offerImage="/img/offer-placeholder-sm.jpg"
              >
                <p className="offer-content-heading">
                  Treningi Personalnie
                </p>
                <p className="offer-content-text">
                  oferujemy treningi personalne z trickingu, które prowadzimy dla osób
                  zarówno młodych jak i starszych. Rozwój na takich treningach jest
                  czysto dobierany pod preferencje jednostki, osoba sama wybiera jakimi
                  gałęziami rozwoju chce kroczyć oraz w jakie elementy celować, naszym
                  zadaniem jest pomoc w tej drodze.
                </p>
                <br /><br />
                <p className="offer-content-heading">
                  Warsztaty oraz zajęcia
                </p>
                <p className="offer-content-text">
                  Jesteśmy trickerami z wieloletnim doświadczeniem w sporcie.
                  Wiemy, że rozwój wykonywanych przez nas elementów musi iść w
                  parze z rozwojem fizycznym ciała. Stąd po za skupianiem się
                  na nauce czysto atletycznych elementów trickingu, takich jak
                  kopnięcia, śruby salta. Kładziemy duży nacisk na rozciąg ciała,
                  jego siłę, dynamikę oraz jego wytrzymałość.
                </p>
              </OurOfferSection>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default Homepage;
