import React, { Component } from 'react';
import PropTypes from 'prop-types';

class OurPartners extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
  }

  render() {
    return (
      <div className="partners-wrapper">
        <div className="partner-item">
          <img
            className="partner-logo"
            src="/img/svg/hangar646.svg"
            alt="Hangar 646"
          />
        </div>
        <div className="partner-item">
          <img
            className="partner-logo"
            src="/img/svg/koszanski.svg"
            alt="Tomasz Koszański"
          />
        </div>
        <div className="partner-item">
          <img
            className="partner-logo"
            src="/img/svg/gojump.svg"
            alt="GOJump"
          />
        </div>
        <div className="partner-item">
          <img
            className="partner-logo"
            src="/img/svg/arte.svg"
            alt="Arte"
          />
        </div>
        <div className="partner-item">
          <img
            className="partner-logo"
            src="/img/svg/hangar646.svg"
            alt="Hangar 646"
          />
        </div>
        <div className="partner-item">
          <img
            className="partner-logo"
            src="/img/svg/koszanski.svg"
            alt="Tomasz Koszański"
          />
        </div>
      </div>
    );
  }
}

OurPartners.contextTypes = {
  currentMedia: PropTypes.string,
};

export default OurPartners;
