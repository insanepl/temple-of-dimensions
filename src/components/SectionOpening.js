import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SectionOpening extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
  }

  render() {
    return (
      <div
        className="section-opening"
      >
        <h3 className="section-title">
          {this.props.sectionTitle}
        </h3>
        <h4 className="section-subheading">
          {this.props.children}
        </h4>
      </div>
    );
  }
}

SectionOpening.propTypes = {
  sectionTitle: PropTypes.string,
  children: PropTypes.any,
};

SectionOpening.contextTypes = {
  currentMedia: PropTypes.string,
};

export default SectionOpening;
