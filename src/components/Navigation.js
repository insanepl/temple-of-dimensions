import { Link } from 'react-router-dom';
import { Motion, spring } from 'react-motion';
import React, { Component } from 'react';
import springPresets from '../springPresets';
import scrollToElement from 'scroll-to-element';
import PropTypes from 'prop-types';
import $ from 'jquery';

class Navigation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isMenuOpen: false,
    };
  }

  menuHandler = () => {
    this.setState({
      isMenuOpen: !this.state.isMenuOpen,
    });
    $('body').toggleClass("is-menu-open");
  };

  scrollToMedia = () => {
    $('body').removeClass("is-menu-open");
    this.setState({
      isMenuOpen: false,
    });
    scrollToElement('.media-section', {
      duration: 800
    });
  };

  scrollToPartners = () => {
    $('body').removeClass("is-menu-open");
    this.setState({
      isMenuOpen: false,
    });
    scrollToElement('.partners-section', {
      duration: 800
    });
  };

  scrollToTeam = () => {
    $('body').removeClass("is-menu-open");
    this.setState({
      isMenuOpen: false,
    });
    scrollToElement('.trickers-section', {
      duration: 800
    });
  };

  render() {
    return (
      <div
        className="dt-navigation"
        role="navigation"
      >
        <Link
          to="/"
          title="Powrót do strony głównej"
          className="go-to-home"
          tabIndex="0"
        >
          <img
            className="dt-logotype"
            src="/img/svg/logotype.svg"
            alt="Temple of Dimensions"
            width="43"
            height="46"
          />
        </Link>
        {this.state.isMenuOpen && (
          <Motion
            defaultStyle={{
              height: 0,
              opacity: 0,
            }}
            style={{
              height: spring(100, springPresets.gentleNoWobble),
              opacity: spring(1, springPresets.gentleNoWobble),
            }}
          >{style => (
            <ul
              className="main-menu"
              style={{
                height: `${style.height}vh`,
                opacity: style.opacity,
              }}
            >
              <li className="main-menu-item">
                <a
                  className="main-menu-anchor"
                  href="/files/oferta.pdf"
                  download
                  data-num="I."
                >
                  Oferta
                </a>
              </li>
              <li className="main-menu-item">
                <p
                  className="main-menu-anchor"
                  data-num="II."
                  onClick={this.scrollToMedia}
                >
                  Media
                </p>
              </li>
              <li className="main-menu-item">
                <p
                  className="main-menu-anchor"
                  data-num="III."
                  onClick={this.scrollToPartners}
                >
                  Partnerzy
                </p>
              </li>
              <li className="main-menu-item">
                <p
                  className="main-menu-anchor"
                  data-num="IV."
                  onClick={this.scrollToTeam}
                >
                  Trickerzy
                </p>
              </li>
            </ul>
          )}
          </Motion>
        )}
        <span
          className="menu-trigger"
          onClick={this.menuHandler}
          tabIndex="1"
        >
          {!this.state.isMenuOpen ? (
            'menu'
          ) : (
            'close'
          )}
          </span>
      </div>
    );
  }
}

Navigation.propTypes = {
  currentMedia: PropTypes.string,
};

Navigation.contextTypes = {
  currentMedia: PropTypes.string,
};

export default Navigation;
