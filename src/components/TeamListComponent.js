import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TeamMember from '../components/TeamMember';

class TeamListComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
  }

  render() {
    return (
      <div className="team-members-wrapper">
        <div className="team-column">
          <TeamMember
            memberName="Bartek Ciszewski"
            memberOrigin="Łódź"
            imageSlug="bartek-ciszewski"
          />
        </div>
        <div className="team-column">
          <TeamMember
            memberName="Mateusz Jazowski"
            memberOrigin="Warszawa"
            imageSlug="mateusz-jazowski"
          />
        </div>
        <div className="team-column">
          <TeamMember
            memberName="Piotr Snow"
            memberOrigin="Kraków"
            imageSlug="piotr-snow"
          />
        </div>
        <p
          className="button primary uppercase ghosty-hover"
        >
          Zobacz wszystkich
        </p>
      </div>
    );
  }
}

TeamListComponent.contextTypes = {
  currentMedia: PropTypes.string,
};

export default TeamListComponent;
