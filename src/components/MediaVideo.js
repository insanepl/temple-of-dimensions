import React, { Component } from 'react';
import isMediaGreaterThan from '../utils/isMediaGreaterThan';
import isMediaLessThan from '../utils/isMediaLessThan';
import PropTypes from 'prop-types';

class MediaVideo extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
  }

  render() {
    return (
      <a
        className="portfolio-item"
        target="_blank"
        rel="noopener noreferrer"
        href={this.props.videoUrl}
        title={this.props.videoTitle}
      >
        <div className="portfolio-content">
          <p className="vid-number">
            0{this.props.videoNumber}.
          </p>
          {isMediaGreaterThan('Mobile', this.context.currentMedia) && [
            <p
              className="video-title"
              key="1"
            >
              {this.props.videoTitle}
            </p>,
            <p
              className="portfolio-video-button"
              key="2"
            >
              <span className="portfolio-video-button-label">
                Zobacz
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 7.1 11.4"
                  width="6"
                  height="9"
                  fill="#FDFEFE"
                  aria-hidden="true"
                >
                  <path
                    d="M1.4 11.4L0 10l4.3-4.3L0 1.4 1.4 0l5.7 5.7"
                  />
                </svg>
              </span>
            </p>,
          ]}
        </div>
        {isMediaLessThan('Small', this.context.currentMedia) && (
          <img
            className="portfolio-vid-placeholder"
            src={`/img/videos/${this.props.videoPosterPreview}-md.jpg`}
            alt={this.props.videoTitle}
            role="presentation"
          />
        )}
        {isMediaGreaterThan('Mobile', this.context.currentMedia)
        && (
          <img
            className="portfolio-vid-placeholder"
            src={`/img/videos/${this.props.videoPosterPreview}-xl.jpg`}
            alt={this.props.videoTitle}
            role="presentation"
          />
        )}
        {isMediaGreaterThan('Medium', this.context.currentMedia) && (
          <div className="vid">
            {`/img/videos/prev/${this.props.videoPosterPreview}.mp4`}
          </div>
        )}
      </a>
    );
  }
}

MediaVideo.propTypes = {
  currentMedia: PropTypes.string,
  videoUrl: PropTypes.string,
  videoTitle: PropTypes.string,
  videoNumber: PropTypes.string,
  videoPosterPreview: PropTypes.string,
};

MediaVideo.contextTypes = {
  currentMedia: PropTypes.string,
};

export default MediaVideo;
