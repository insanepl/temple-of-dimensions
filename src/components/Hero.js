import React, { Component } from 'react';
import PropTypes from 'prop-types';
import scrollToElement from 'scroll-to-element';
import SectionOpening from '../components/SectionOpening';

class Hero extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
  }

  scrollToPage = () => {
    scrollToElement('.start-scrolling', {
      duration: 800
    });
  };

  render() {
    return (
      <header
        role="banner"
        className="main-hero"
      >
        <div className="hero-cta">
          <h1
            ref="site-heading"
            className="hero-header"
          >
            <span className="smaller">
              Temple of
            </span>
            <span className="larger">
              Dimensions
            </span>
          </h1>
          <h2
            ref="site-claim"
            className="hero-claim"
          >
            Tricking w najlepszym wydaniu ukazany
            w różnorodności form ruchu.
          </h2>
          <a
            ref="cta-button"
            className="button primary uppercase glitchy"
            href="/files/offer.pdf"
            download
            data-text="Nasza oferta"
            title="Nasza Oferta"
          >
            Nasza oferta
          </a>
        </div>
        <span
          ref="scroller"
          className="scroll-to-page media-section start-scrolling"
          title="Odkryj więcej!"
          onClick={this.scrollToPage}
        >
          Odkryj więcej!
        </span>
        <SectionOpening sectionTitle="Media">
          Znajdziesz tutaj filmy ukazujące zarys naszej pracy,
          filmy promocyjne i tematyczne oraz materiały związane
          z warsztatami, zajęciami i wydarzeniami.
        </SectionOpening>
      </header>
    );
  }
}

Hero.propTypes = {
  currentMedia: PropTypes.string,
};

Hero.contextTypes = {
  currentMedia: PropTypes.string,
};

export default Hero;
