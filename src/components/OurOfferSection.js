import React, { Component } from 'react';
import PropTypes from 'prop-types';

class OurOfferSection extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
  }

  render() {
    return (
      <div
        className={`offer-box ${this.props.className}`}
      >
        <div className="offer-image-wrapper">
          <h5 className="offer-heading">
            Oferta
            <br />
            {this.props.offerType}
          </h5>
          <img
            className="offer-box-image"
            role="presentation"
            alt={`Oferta ${this.props.offerType}`}
            src={this.props.offerImage}
          />
        </div>
        <div className="offer-content-box">
          {this.props.children}
        </div>
      </div>
    );
  }
}

OurOfferSection.contextTypes = {
  currentMedia: PropTypes.string,
};

OurOfferSection.propTypes = {
  className: PropTypes.string,
  offerType: PropTypes.string,
  offerImage: PropTypes.string,
  children: PropTypes.any,
};

export default OurOfferSection;
