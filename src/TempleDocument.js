import React, { Component } from 'react';
import MediaDetectElement from './utils/MediaDetectElement';
import Navigation from './components/Navigation';
import PropTypes from 'prop-types';
import $ from 'jquery';

class TempleDocument extends Component {
  static contextTypes = {
    currentMedia: PropTypes.string,
  };

  static propTypes = {
    children: PropTypes.node.isRequired,
    params: PropTypes.object,
  };

  static childContextTypes = {
    currentMedia: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      currentMedia: null,
    };
  }

  getChildContext() {
    return {
      currentMedia: this.state.currentMedia,
    };
  }

  onMediaChange = (currentMedia) => {
    this.setState({ currentMedia });
  };

  componentDidMount() {
    window.addEventListener('scroll', this.stickyScroll, false);
  }

  stickyScroll() {
    if( window.pageYOffset > 200 ) {
      $(document.body).addClass('is-scrolling');
    }

    if( window.pageYOffset < 540 ) {
      $(document.body).removeClass('is-scrolling');
    }
  };

  render() {
    return (
      <div className="main-app">
        <Navigation
          currentMedia={this.state.currentMedia}
        />
        {this.props.children}
        <MediaDetectElement
          aria-hidden="true"
          onMediaChange={this.onMediaChange}
        />
      </div>
    );
  }
}

TempleDocument.propTypes = {
  location: PropTypes.object,
  children: PropTypes.any,
};

TempleDocument.contextTypes = {
  router: PropTypes.object,
};

TempleDocument.childContextTypes = {
  currentMedia: PropTypes.string,
};

export default TempleDocument;
